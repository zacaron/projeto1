/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author zacaron
 */
public class Poligonal <T>{
    
    //
        private T[] vertices;
    //    
    //private Ponto2D[] vertices;
    
    
 
    public Poligonal() {
    }
    
    
    //public Poligonal(Ponto2D[] vertices) {
    public Poligonal(T[] vertices) {
        if(vertices.length < 2 ) {
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        } else {
            this.vertices = vertices;
        }
    }

    
    public int getN() {
        return getVertices().length;
    }
    
    //public Ponto2D get(int c1) {
    public T get(int c1) {
        return vertices[c1];
    }
    
    //public void set(int c1, Ponto2D vertices) {
    public void set(int c1, T vertices) {
        this.vertices[c1] = vertices;
    }
    
    public double getComprimento() {
        double comp = 0;
        
        Ponto p1 = (Ponto) vertices[0];
        //for (Ponto2D p2 : vertices) {
        for (T p2 : vertices) {
            comp += p1.dist((Ponto) p2);
            p1 = (Ponto) p2;
        }
        return comp;
    }
  
    //public Ponto2D[] getVertices() {
    public T[] getVertices() {
        return vertices;
    }

    //public void setVertices(Ponto2D[] vertices) {
    public void setVertices(T[] vertices) {
        this.vertices = vertices;
    }
    
 
    public String getNome() {
        return getClass().getSimpleName();
    }
    
}

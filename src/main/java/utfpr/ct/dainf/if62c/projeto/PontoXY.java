/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author zacaron
 */
public class PontoXY extends Ponto2D{
    
    private double x, y;

    public PontoXY() {
    }

    public PontoXY(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    
    @Override
    public double getX() {
        return x;
    }
    
    @Override
    public void setX(double x) {
        this.x = x;
    }
    
    @Override
    public double getY() {
        return y;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }
    
    @Override
    public String toString() {
        return String.format(getNome() +"(%f,%f)", getX(), getY());
    }
}

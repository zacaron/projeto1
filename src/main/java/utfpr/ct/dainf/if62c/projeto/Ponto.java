package utfpr.ct.dainf.if62c.projeto;

import javafx.scene.effect.Light;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    
    private double x, y, z;

        public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    
    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    
    

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName()+x+y+z;
    }
    
    /*public String getNome() {
    return this.getClass().getSimpleName();
    }*/

    @Override
    public boolean equals(Object x) {
        return super.equals(y); //To change body of generated methods, choose Tools | Templates.
    }
    
    /* item 6
    public double dist(x, y){
        return (x -y);
    }
    */
    
    public boolean equals(Ponto p1) {
        if(p1 instanceof Ponto) {
            if(p1 instanceof PontoXY) {
                return this.getX() == p1.getX() && this.getY() == p1.getY();
            }
            if(p1 instanceof PontoXZ) {
                return this.getX() == p1.getX() && this.getZ() == p1.getZ();
            }
            if(p1 instanceof PontoYZ) {
                return this.getY() == p1.getY() && this.getZ() == p1.getZ();
            }
        } 
        return false;
    }
    
    public double dist(Ponto p1) {
        return Math.sqrt(Math.pow(p1.getX() - this.getX(), 2) + Math.pow(p1.getY() - this.getY(), 2) + Math.pow(p1.getZ() - this.getZ(), 2));
    }

    
    
    
    
}

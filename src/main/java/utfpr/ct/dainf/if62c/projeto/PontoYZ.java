/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author zacaron
 */
public class PontoYZ extends Ponto2D{
    
    private double y, z;

    public PontoYZ() {
    }

    public PontoYZ(double y, double z) {
        this.y = y;
        this.z = z;
    }
    
    
    @Override
    public double getY() {
        return y;
    }
    @Override
    public void setY(double y) {
        this.y = y;
    }
    @Override
    public double getZ() {
        return z;
    }
    @Override
    public void setZ(double z) {
        this.z = z;
    }
    
    
    @Override
    public String toString() {
        return String.format(getNome() +"(%f,%f)", getY(), getZ());
    }
    
}

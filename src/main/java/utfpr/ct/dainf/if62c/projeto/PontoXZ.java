/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author zacaron
 */
public class PontoXZ extends Ponto2D{
    
    private double x, z;
    @Override
    public double getX() {
        return x;
    }
    @Override
    public void setX(double x) {
        this.x = x;
    }
    @Override
    public double getZ() {
        return z;
    }
    @Override
    public void setZ(double z) {
        this.z = z;
    }


    public PontoXZ(double x, double z) {
        this.x = x;
        this.z = z;
    }

    public PontoXZ() {
    }
    
    @Override
    public String toString() {
        return String.format(getNome() +"(%f,%f)", getX(), getZ());
    }
    
    
    
    
}
